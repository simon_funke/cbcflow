# Copyright (C) 2010-2014 Simula Research Laboratory
#
# This file is part of CBCFLOW.
#
# CBCFLOW is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CBCFLOW is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with CBCFLOW. If not, see <http://www.gnu.org/licenses/>.
from cbcflow.fields.bases.MetaPPField import MetaPPField
from numpy import sqrt

class RunningL2norm(MetaPPField):
    def before_first_compute(self, pp, spaces, problem):
        self._count = 0
        self._sum = 0
        self._value = 0

    def compute(self, pp, spaces, problem):
        u = pp.get(self.valuename)

        self._count += 1
        self._sum += u**2
        self._value = sqrt(self._sum) / self._count

        return self._value
